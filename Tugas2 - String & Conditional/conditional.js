//If-else 
var nama = "";  // isi nama
var peran = ""; // isi peran

var space = "\n";

if (nama == "") {
    console.log("Nama harus diisi!");
} else if (nama.length > 0 && peran.length < 1){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else if (nama.length > 0 && peran.toLowerCase() == "penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama );
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama.length > 0 && peran.toLowerCase() == "guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama );
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama.length > 0 && peran.toLowerCase() == "werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama );
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
} else {
    console.log("Ulangi pengisian. Tidak ditemukan peran " + peran);
}
console.log(space);

//Switch Case
var hari = 3;      // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 11;    // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2020;  // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan) {
    case 1:
        console.log("Sekarang Tanggal " + hari + " " + "Januari" + " " + tahun);
      break;
    case 2:
        console.log("Sekarang Tanggal " + hari + " " + "Februari" + " " + tahun);
      break;
    case 3:
        console.log("Sekarang Tanggal " + hari + " " + "Maret" + " " + tahun);
      break;
    case 4:
        console.log("Sekarang Tanggal " + hari + " " + "April" + " " + tahun);
      break;
    case 5:
        console.log("Sekarang Tanggal " + hari + " " + "Mei" + " " + tahun);
      break;
    case 6:
        console.log("Sekarang Tanggal " + hari + " " + "Juni" + " " + tahun);
      break;
    case 7:
        console.log("Sekarang Tanggal " + hari + " " + "Juli" + " " + tahun);
      break;
    case 8:
        console.log("Sekarang Tanggal " + hari + " " + "Agustus" + " " + tahun);
      break;
    case 9:
        console.log("Sekarang Tanggal " + hari + " " + "September" + " " + tahun);
      break;
    case 10:
        console.log("Sekarang Tanggal " + hari + " " + "Oktober" + " " + tahun);
      break;
    case 11:
        console.log("Sekarang Tanggal " + hari + " " + "November" + " " + tahun);
      break;
    case 12:
        console.log("Sekarang Tanggal " + hari + " " + "Desember" + " " + tahun);
      break;
    default:
        console.log("Tidak ada bulan " + bulan);
      break;
  }

