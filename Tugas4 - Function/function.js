// No. 1 (tanpa return)
console.log("\n")
console.log("Output no 1")
function teriak() {
    console.log("Halo Sanbers!");
}
   
teriak(); 


// No. 2 
console.log("\n")
console.log("Output no 2")
function kalikan() {
    var kalikan = num1 * num2;
    return kalikan;
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48


// No. 3 
console.log("\n")
console.log("Output no 3")
function introduce(name, age, address, hobby) {
    var kalimat = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby;
    return kalimat;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"
