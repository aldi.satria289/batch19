//No. 1 Looping While
var love = 2;
var space = "";

console.log(space);
console.log("Looping pertama");
while(love < 21) { // Loop akan terus berjalan selama nilai love masih dibawah 20
  if ( love%2 != 1){ //Kelipatan 2 (nilai bukan kelipatan dari 1 atau genap) supaya ganjil pakai == 1
  console.log(love + " - I love coding"); // Menampilkan nilai love pada iterasi tertentu
    }  love++; // Mengubah nilai love dengan menambahkan 1
}

var love = 20;
console.log(space);
console.log("Looping kedua");
while(love > 0) { // Loop akan terus berjalan selama nilai love masih diatas 0
    if ( love%2 != 1){
    console.log(love + " - I will become a mobile developer"); // Menampilkan nilai love pada iterasi tertentu
      }  love--; // Mengubah nilai love dengan mengurangi 1
  }



//No. 2 Looping menggunakan for
console.log(space);
console.log("Output Soal No 2");
for(var angka = 1; angka < 21; angka++) { 
    if ( angka%2 == 1){     
        if ( angka%3 == 0){ 
            console.log(angka + " - I love coding"); 
        } else (            
            console.log(angka + " - Santai"));      
    } else (
        console.log(angka + " - Berkualitas"));      
} 

//No. 3 Membuat Persegi Panjang
console.log(space);
console.log("Output Soal No 3");

for (var i=1;i<=4;i++){     
    var simpan = "";        
    for (var j=1;j<=7;j++){ 
        simpan += "#";
    }
    console.log(simpan);
}

//No. 4 Membuat Tangga
console.log(space);
console.log("Output Soal No 4");

for (var i=1;i<=7;i++){
    var simpan = ""; 
    for (var j=1;j<=i;j++){
        simpan += "#";
    }
    console.log(simpan);
}


//No. 5 Membuat Papan Caturg
console.log(space);
console.log("Output Soal No 5");
var baris = 8;
var kolom = 8;
 
    for(var i = 0; i < baris; i++){
      var isi = i % 2 === 1 ? ' ' : '#';
      var simpan = '';

      for(var j = 0; j < kolom; j++){       
        isi = isi == '#' ? ' ' : '#';
        simpan += isi;
      }   
     
      console.log(simpan);
    }
  