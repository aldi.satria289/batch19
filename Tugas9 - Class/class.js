//1. Animal Class
//Release 0
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
    present() {
        return "Namaku " + this.name + "\nLegs : " + this.legs + "\nColdblooded : "  + this.cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log("Namaku " + sheep.name) // "shaun"
console.log("Legs : " + sheep.legs) // 4
console.log("Coldblooded : " + sheep.cold_blooded) // false
console.log("")

//Release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name,legs) {
        super(name);
        this.legs = 2;
    } 
    yell() {
        console.log(this.present());
        console.log("Auooo");
    }
} 

class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        console.log(this.present());
        console.log("hop hop");
    }
} 

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
console.log("")
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log("")



//2. Function to Class

/*
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}
*/


class Clock {
    constructor({template}){
        this.template = template
    }
    render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }
}

  var clock = new Clock({template: 'h:m:s'});
  console.log("Jam dimulai")
  clock.start(); 


  