var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

async function asyncCall() {
    let waktu = 10000
    for (let i = 0; i < books.length; i++) {
        waktu = await readBooksPromise(waktu, books[i])
            .then((sisaWaktu) => {
                return sisaWaktu;
            })
            .catch((sisaWaktu) => {
                return sisaWaktu;
            });
    }
    console.log("selesai")
}

console.log(`Output no 2`)
asyncCall();