import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import Component from './Latihan/Component/Component';
import LatihanComponent from './Latihan/Component/Component'
import YoutubeUI from './Tugas/Tugas12-Component/App'
import LatihanStyle from './Latihan/Styling/index'
import LatihanStyle2 from './Latihan/Styling/Props'
import LoginScreen from './Tugas/Tugas13-Styling/LoginScreen'
import AboutScreen from './Tugas/Tugas13-Styling/AboutScreen'
import TodoApp from './Tugas/Tugas14-APILifecycle/Main'
import LatihanNavi from './Latihan/Navigation/index'
import Tugas15 from './Tugas/Tugas15-Navigation/index'
import TugasNavi from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'

export default function App() {
  return (
    <Quiz3/>
    // <TugasNavi/>
    // <Tugas15/>
    // <LatihanNavi/>
    // <TodoApp/> //Tugas14
    // <AboutScreen/> //Tugas13
    // <LoginScreen/> //Tugas13
    // <YoutubeUI/> //Tugas12
    // <LatihanComponent/>
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
