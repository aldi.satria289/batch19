import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
            <Image source={require('./images/logo.png')} style={{marginTop:100,width:375, height:102}}/>
            <Text style={styles.headerText}>PORTOFOLIO</Text>
            <Text style={styles.loginText}>Login</Text>
            <View style={{marginTop:40}}>
                <Text style={styles.nameBox}>Username / Email</Text>
                <TouchableOpacity>
                <View style={styles.textBox} /></TouchableOpacity>
                <Text style={styles.nameBox}>Password</Text>
                <TouchableOpacity>
                <View style={styles.textBox} /></TouchableOpacity>
            </View>
            <TouchableOpacity>
            <Text style={styles.loginBox}>Masuk</Text></TouchableOpacity>
            <Text style={styles.loginText}>atau</Text>
            <TouchableOpacity>
            <Text style={styles.registerBox}>Daftar ?</Text></TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    marginTop: -20,
    marginLeft: 200,
    marginBottom: 50,
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#3EC6FF'
  },
  loginText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#003366'
  },
  nameBox: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#003366'
  },
  textBox: {
    width: 300,
    height:50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#003366',
    marginBottom: 20,
  },
  loginBox: {
    width: 140,
    height:40,
    fontFamily: 'Roboto',
    fontSize: 24,
    textAlign: "center",
    paddingTop: 3,
    backgroundColor: '#ffd740',
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 16,
    margin: 15,
  },
  registerBox: {
    width: 140,
    height:40,
    fontFamily: 'Roboto',
    color: 'white',
    fontSize: 24,
    textAlign: "center",
    paddingTop: 3,
    backgroundColor: '#003366',
    borderWidth: 1,
    borderColor: '#003366',
    borderRadius: 16,
    margin: 15,
  },
})