import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'; 

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
            <Text style={styles.headerText}>Tentang Saya</Text>
            <FontAwesome name="user-circle" size={200} color="#aaa" />
            <Text style={styles.textNama}>Albert Einstein</Text>
            <Text style={styles.textSkill}>Mad Scientist</Text>
            <View style={{marginTop:20}}>
                <View style={styles.portoBox}>
                    <Text style={styles.nameBox}>Portofolio</Text>
                    <View style={styles.portoIsi}>
                        <View style={styles.portoItem}>
                            <FontAwesome name="gitlab" size={40} color="#3EC6FF" />
                            <Text style={styles.portoNama}>@einsteincuy</Text>
                        </View>
                        <View style={styles.portoItem}>
                            <FontAwesome name="github" size={40} color="#3EC6FF" />
                            <Text style={styles.portoNama}>@einstein.cuy</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={{marginTop:10}}>
                <View style={styles.contactBox}>
                <Text style={styles.nameBox}>Hubungi Saya</Text>
                <View style={styles.kontakIsi}>
                        <View style={styles.kontakItem}>
                            <View style={{width:60,alignItems: 'center'}}>
                                <FontAwesome name="facebook" size={40} style={{margin:10}} color="#3EC6FF" />
                            </View>
                            <View style={{ justifyContent: 'center'}}>
                                <Text style={styles.kontakNama}>einstein.tampan</Text>
                            </View>
                        </View>
                        <View style={styles.kontakItem}>
                            <View style={{width:60,alignItems: 'center'}}>
                                <FontAwesome name="instagram" size={40} style={{margin:10}} color="#3EC6FF" />
                            </View>
                            <View style={{ justifyContent: 'center'}}>
                                <Text style={styles.kontakNama}>@einstein.jenius</Text>
                            </View>
                        </View>
                        <View style={styles.kontakItem}>
                            <View style={{width:60,alignItems: 'center'}}>
                            <FontAwesome name="twitter" size={40} style={{margin:10}} color="#3EC6FF" />
                            </View>
                            <View style={{justifyContent: 'center'}}>
                                <Text style={styles.kontakNama}>@einstein.imoet</Text>
                            </View>
                        </View>
                </View>
                </View>
            </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    marginTop: 60,
    marginBottom: 20,
    fontFamily: 'Roboto',
    fontSize: 36,
    fontWeight: 'bold',
    color: '#003366'
  },
  textNama: {
    marginTop: 20,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 24,
    color: '#003366'
  },
  textSkill: {
    marginTop: 5,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#3EC6FF'
  },
  portoBox: {
    width: 360,
    height:140,
    backgroundColor: '#ddd',
    borderRadius: 16,
    padding: 10,
  },
  contactBox: {
    width: 360,
    height:250,
    backgroundColor: '#ddd',
    borderRadius: 16,
    padding: 10,
  },
  nameBox: {
    paddingBottom: 5,
    fontFamily: 'Roboto',
    borderBottomColor: '#003366',
    borderBottomWidth: 1,
    fontSize: 18,
    color: '#003366'
  },
  portoIsi: {
    height:60,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  portoItem: {
    alignItems:'center',
    justifyContent:'center'
  },
  portoNama: {
    fontSize: 16,
    color: '#003366',
    fontWeight: 'bold',
    paddingTop: 4
  },
  kontakIsi: {
    marginTop:20,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  kontakItem: {
    flexDirection: 'row',
    justifyContent:'center'
  },
  kontakNama: {
    fontSize: 16,
    color: '#003366',
    fontWeight: 'bold',
    paddingTop: 4,
  }
})