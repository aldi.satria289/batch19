import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    height:100,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    flexDirection: 'column',
    justifyContent: 'space-between',
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Details = ({route}) => (
  <ScreenContainer>
    <Text style={{marginBottom:10}}>Detail Halaman</Text>
    {route.params.name && <Text>{route.params.name}</Text>}
  </ScreenContainer>
);

export const Home = ({navigation}) => (
  <ScreenContainer>
    <Text>Halaman Skill</Text>
    <View style={styles.button}>
    <Button title="Lihat Skill" onPress={() => navigation.push("Details", {name: "Ini Skill Gw"})} />
    <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
    </View>
  </ScreenContainer>
);

export const Project = ({navigation}) => (
  <ScreenContainer>
    <Text>Halaman Proyek</Text>
    <View style={styles.button}>
    <Button title="Lihat Project" onPress={() => navigation.push("Details", {name: "Ini Project Gw"})} />
    <Text></Text>
    <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
    </View>
  </ScreenContainer>
);

export const Add = ({navigation}) => (
  <ScreenContainer>
    <Text>Halaman Tambah</Text>
    <View style={styles.button}>
    <Button title="Lihat Add" onPress={() => navigation.push("Details", {name: "Ini Add Gw"})} />
    <Text></Text>
    <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
    </View>
  </ScreenContainer>
);

