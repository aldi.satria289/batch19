import React from "react";
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {enableScreens} from 'react-native-screens';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';

import {Home, Details, Project, Add} from './Screen'

import { StyleSheet, Text, View } from "react-native";

enableScreens();

const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} options={{ headerTitleAlign: 'center' }}/>
    <HomeStack.Screen 
      name="Details" 
      component={Details} 
      options={({ route }) => ({
        title: route.params.name
      })}/>
  </HomeStack.Navigator>
)

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} options={{ headerTitleAlign: 'center' }}/>
    <ProjectStack.Screen 
      name="Details" 
      component={Details} 
      options={({ route }) => ({
        title: route.params.name
      })}/>
  </ProjectStack.Navigator>
)

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add} options={{ headerTitleAlign: 'center' }}/>
    <AddStack.Screen 
      name="Details" 
      component={Details} 
      options={({ route }) => ({
        title: route.params.name
      })}/>
  </AddStack.Navigator>
)

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Sanber App" component={LoginScreen} options={{ headerTitleAlign: 'center' }}/>
  </LoginStack.Navigator>
)

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={AboutScreen} options={{ headerTitleAlign: 'center' }}/>
  </AboutStack.Navigator>
)


const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={HomeStackScreen}/>
    <Tabs.Screen name="Project" component={ProjectStackScreen}/>
    <Tabs.Screen name="Add" component={AddStackScreen}/>
  </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={TabsScreen}/>
    <Drawer.Screen name="About" component={AboutStackScreen}/>
    <Drawer.Screen name="Login" component={LoginStackScreen}/>
  </Drawer.Navigator>
)

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Login" component={LoginStackScreen}/>
    <RootStack.Screen name="Drawer" component={DrawerScreen} />
  </RootStack.Navigator>
);

export default () => 
  (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});