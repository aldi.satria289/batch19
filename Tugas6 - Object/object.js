//Soal No. 1 (Array to Object)

function arrayToObject (arr){
    
    for(var i=0; i<arr.length; i++){
        var objectBaru = {};
        var tahunLahir = arr[i][3];
        var tahunSekarang = new Date().getFullYear()
        var umur;
        
        if (tahunLahir && tahunSekarang - tahunLahir > 0){
            umur = tahunSekarang - tahunLahir
        }else{
            umur ="invalid Birth Year"
        }
        objectBaru.firstName = arr[i][0]
        objectBaru.lastName = arr[i][1]
        objectBaru.gender= arr[i][2]
        objectBaru.age = umur

        var namaObject = (i + 1) + '. ' + 
                         objectBaru.firstName + ' ' +
                         objectBaru.lastName + ' : ';

        console.log(namaObject)
        console.log(objectBaru)
        console.log("")
    
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log("---Output Soal no 1---")
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
// Error case 
arrayToObject([]) // ""
console.log("")



//Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }else {
        var objectBaru ={};
        var sisaUang = money;
        var daftarBelanjaan = [];
        var belanja = ["Sepatu Stacatu","Baju Zoro","Baju H&N","Sweater Unikloh","Casing Handphone"];
        var check = 0;
        for (var i = 0; sisaUang >= 50000  && check ==0; i++){
            if( sisaUang >= 1500000){
                daftarBelanjaan.push(belanja[0])
                sisaUang -= 1500000
            }else if( sisaUang >= 500000){
                daftarBelanjaan.push(belanja[1])
                sisaUang -= 500000
            }else if( sisaUang >= 250000){
                daftarBelanjaan.push(belanja[2])
                sisaUang -= 250000
            }else if( sisaUang >= 175000){
                daftarBelanjaan.push(belanja[3])
                sisaUang -= 175000
            }else if( sisaUang >= 50000){
               for (var j= 0; j<= daftarBelanjaan.length-1; j++){
                   if(daftarBelanjaan[j]== belanja[4]){
                       check +=1
                   }
               }if(check==0){
                daftarBelanjaan.push(belanja[4])
                   sisaUang -= 50000
               }
            }
            
        }
        
        objectBaru.memberId = memberId;
        objectBaru.money = money;
        objectBaru.listPurchased = daftarBelanjaan;
        objectBaru.changeMoney = sisaUang;
        return objectBaru
    }
  }
   
  // TEST CASES
  console.log("---Output Soal no 2---")
  
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
  console.log("");



  //Soal No. 3 (Naik Angkot)
  function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrayBaru = []
    if (arrPenumpang.length <= 0) {
      return []
    }
  
    for (var i = 0; i < arrPenumpang.length; i++) {
      var objectBaru = {}
      var asal = arrPenumpang[i][1]
      var tujuan = arrPenumpang[i][2]
  
      var indexAsal;
      var indexTujuan;
  
      for (var j = 0; j < rute.length; j++) {
        if (rute[j] == asal) {
          indexAsal = j
        } else if (rute[j] == tujuan) {
          indexTujuan = j
        }
      }
  
      var bayar = (indexTujuan - indexAsal) * 2000
  
      objectBaru.penumpang = arrPenumpang[i][0]
      objectBaru.naikDari = asal
      objectBaru.tujuan = tujuan
      objectBaru.bayar = bayar
  
      arrayBaru.push(objectBaru)
    }
  
    return arrayBaru
  }
   
  //TEST CASE
  console.log("---Output Soal no 3---")
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  //   { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]


