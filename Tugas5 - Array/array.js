//Soal No. 1 (Range)
function range(startNum, finishNum) {
    var jarak = [];
    if (startNum < finishNum){
        for (var i = startNum; i <= finishNum; i++) 
        jarak.push(i);
    }else if (startNum > finishNum) {
        for (var i = 0; i < startNum - finishNum + 1; i++) 
        jarak.push(startNum - i);
    }else if (!startNum || !finishNum){
        return -1
    }   
    return jarak;
}

console.log("------Output Soal 1")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("")

//Soal No. 2 (Range with Step)
 function rangeWithStep (startNum, finishNum, step){
     var jarak = [];
     if (startNum < finishNum){
        for(var i = startNum; startNum <= finishNum; i++){
            jarak.push(startNum)
           startNum += step
        }
     }else if (startNum > finishNum){
        for (var i = 0; startNum >= finishNum; i++){
            jarak.push(startNum)
           startNum -= step
        }
     }else if( !startNum || !finishNum || step){
         return -1
     }

     return jarak

 }

 console.log("------Output Soal 2")
 console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
 console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
 console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
 console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
 console.log("")
 

//Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step){
    var jarak = [ ];
    var langkah;

    if(!step){
        langkah=1
    }else {
        langkah = step
    }

    if(startNum < finishNum){
        for (var i = 0; startNum <=finishNum; i++){
            jarak.push(startNum)
            startNum += langkah
        }
    }else if(startNum > finishNum){
        for (var i = 0; startNum >=finishNum; i++){
            jarak.push(startNum)
            startNum -= langkah
        }
    } else if(!startNum && !finishNum && !step){
        return 0
        
    } else if (startNum){
        return startNum
    }

    var hasilsum = 0;
    for (var i = 0; i< jarak.length; i++){
        hasilsum= hasilsum + jarak[i]
    }
    return hasilsum

}

console.log("------Output Soal 3")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("")


//Soal No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(data) {
    var txt = "";
    for (n in data) {
        txt += "Nomor ID:  " + input[n][0] + 
               "\nNama Lengkap:  " + input[n][1] + 
               "\nTTL:  " + input[n][2] + ", " + input[n][3] + 
               "\nHobi:  " + input[n][4] + "\n\n";             
    }
    
    return txt;
}

var hasildata = dataHandling(input);

console.log("------Output Soal 4")
console.log(hasildata);


//Soal No. 5 (Balik Kata)
function balikKata(kata) {

    var panjangkata = kata.length;
    var i = 0;
    var hasilbalik = "";
    while (i < panjangkata) {
        hasilbalik = hasilbalik + kata[panjangkata - i - 1];
        i += 1;
    }
    return hasilbalik;

}

console.log("------Output Soal 5")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("")


//Soal No. 6 (Metode Array)
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
dataHandling2(input);

function dataHandling2(data){
    var databaru = data
    var tambahnama = data[1] + " Elsharawy"
    var tambahprovinsi ="Provinsi " + data[2]
    var gender ="Pria"
    var institusi = "SMA International Metro"

    databaru.splice(1, 1, tambahnama)
    databaru.splice(2, 1, tambahprovinsi)
    databaru.splice(4, 1, gender, institusi)

    var tanggal = data [3]
    var tanggalbaru = tanggal.split('/')
    var bulan = tanggalbaru[1]
    var namabulan=" "

    switch( bulan){
    case "01":
        namabulan ="Januari"
        break;
    case "02":
        namabulan ="Februari"
        break;
    case "03":
        namabulan ="Maret"
        break;
    case "04":
        namabulan ="April"
        break;
    case "05":
        namabulan ="Mei"
        break;
    case "06":
        namabulan ="Juni"
        break;
    case "07":
        namabulan ="Juli"
        break;
    case "08":
        namabulan ="Agustus"
        break;
    case "09":
        namabulan ="September"
        break;
    case "10":
        namabulan ="Oktober"
        break;
    case "11":
        namabulan ="November"
        break;
    case "12":
        namabulan ="Desember"
        break;
    default:
        break;
    }
    
    var tanggaldijoin = tanggalbaru.join("-")    
    var namadislice= tambahnama.slice(0, 15)
    var tanggaldisort = tanggalbaru.sort(function(a, b){
        return b - a
    })

console.log("------Output Soal 6")
console.log(databaru)
console.log(namabulan)
console.log(tanggaldisort)
console.log(tanggaldijoin)
console.log(namadislice)
}


