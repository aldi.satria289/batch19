//1. Mengubah fungsi menjadi fungsi arrow

/*
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
golden()
*/

//ES6
console.log("--Output no 1--")
const golden = goldenFunction = () =>{
    console.log("this is golden!!")
  }
   
golden();

console.log("")

//2. Sederhanakan menjadi Object literal di ES6
/*
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }

//Driver Code 
newFunction("William", "Imoh").fullName() 
*/

const newFunction = (firstName, lastName) => {
    return {
        fullName() {
            console.log(firstName + " " + lastName)
        }
    }
}

   
//Driver Code 
console.log("--Output no 2--")  
newFunction("William", "Imoh").fullName() 

console.log("")

//3. Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
*/

const {firstName, lastName, destination, occupation} = newObject;

// Driver code
console.log("--Output no 3--")  
console.log(firstName, lastName, "dari", destination, "pekerjaan", occupation)

console.log("")

//4. Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//const combined = west.concat(east)

const combined = [...west, ...east]

//Driver Code
console.log("--Output no 4--") 
console.log(combined)

console.log("")

//5. Template Literals
const planet = "earth"
const view = "glass"
/*
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
*/

let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log("--Output no 5--") 
console.log(before)